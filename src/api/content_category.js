/**
 * api代码文件
 * 注意: //addCode 下方为自动生成的代码，不可删除该注释
 */
import request from '@/utils/request'
// addCode
// removeContent_category Start
// 框架与分类-删除
export function removeContent_category(params) {
    let _params = {

        _id: params._id // 数据唯一id
    }
    return request({
        url: '/content_category/delete',
        method: 'post',
        data: _params
    })
}
// removeContent_category end

// findWhereContent_category Start
// 框架与分类-分页查询
export function findWhereContent_category(params) {
    let _params = {

        startTime: params.startTime, // 开始日期
        endTime: params.endTime, // 结束日期
        start: params.start, // 分页起始位置
        limit: params.limit, // 分页显示条数
        uid: params.uid, // uid
        name: params.name, // 名称
        keywords: params.keywords, // 关键字
        sortId: params.sortId, // 排序
        parentID: params.parentID, // 父元素id
        state: params.state, // 是否公开
        date: params.date, // 日期
        contentTemp: params.contentTemp, // 内容模板
        defaultUrl: params.defaultUrl, // 父级目录
        homePage: params.homePage, // 页面
        sortPath: params.sortPath, // 所有父节点
        comments: params.comments // 评论
    }
    return request({
        url: '/content_category/list',
        method: 'get',
        params: _params
    })
}
// findWhereContent_category end

// findOneContent_category Start
// 框架与分类-详细信息
export function findOneContent_category(params) {
    let _params = {

        _id: params._id // 数据唯一id
    }
    return request({
        url: '/content_category/data',
        method: 'get',
        params: _params
    })
}
// findOneContent_category end

// updateContent_category Start
// 框架与分类-更新
export function updateContent_category(params) {
    let _params = {

        _id: params._id, // 列表唯一id
        uid: params.uid, // uid
        name: params.name, // 名称
        keywords: params.keywords, // 关键字
        sortId: params.sortId, // 排序
        parentID: params.parentID, // 父元素id
        state: params.state, // 是否公开
        date: params.date, // 日期
        contentTemp: params.contentTemp, // 内容模板
        defaultUrl: params.defaultUrl, // 父级目录
        homePage: params.homePage, // 页面
        sortPath: params.sortPath, // 所有父节点
        comments: params.comments // 评论
    }
    return request({
        url: '/content_category/update',
        method: 'post',
        data: _params
    })
}
// updateContent_category end

// saveContent_category Start
// 框架与分类-新增
export function saveContent_category(params) {
    let _params = {

        uid: params.uid, // uid
        name: params.name, // 名称
        keywords: params.keywords, // 关键字
        sortId: params.sortId, // 排序
        parentID: params.parentID, // 父元素id
        state: params.state, // 是否公开
        date: params.date, // 日期
        contentTemp: params.contentTemp, // 内容模板
        defaultUrl: params.defaultUrl, // 父级目录
        homePage: params.homePage, // 页面
        sortPath: params.sortPath, // 所有父节点
        comments: params.comments // 评论
    }
    return request({
        url: '/content_category/add',
        method: 'post',
        data: _params
    })
}
// saveContent_category end