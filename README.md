<p align="center">
	<img alt="logo" src="https://www.guiplan.com/upload/website/set/zFbTsB6Ct4vzGmc6d8O1JQRhBujrxbgc.png" height=100>
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">koajsAdmin2.0</h1>
<h4 align="center">基于koa2+Vue3前后端分离的node快速开发框架</h4>
<h4 align="center">更多教程： [www.guiplan.com](https://www.guiplan.com) </h4>

## 平台简介

* 本仓库为前端技术栈 [Vue3](https://v3.cn.vuejs.org) + [Element Plus](https://element-plus.org/zh-CN) + [Vite](https://cn.vitejs.dev) 版本。
* 配套后端代码servers文件夹中。
* 前端技术栈（[Vue2](https://cn.vuejs.org) + [Element](https://github.com/ElemeFE/element) + [Vue CLI](https://cli.vuejs.org/zh)），请移步[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue/tree/master/ruoyi-ui)。
* 本仓库基于若依vue3前端框架用guiplan转可视化之后二次开发，配合自己的koa2 + mongodb 实现前后端分离。再次感谢若依团队。
* 配合guiplan软件，可以实现前端可视化开发，以及后端接口的可视化配置。自动生成前后端所有代码。

## 前端运行

```bash
# 克隆项目
git clone https://gitee.com/zhoushuigui/koajs-admin.git

# 进入项目目录
cd koajs-admin

# 安装依赖
yarn --registry=https://registry.npmmirror.com

# 启动服务
yarn dev

# 构建测试环境 yarn build:stage
# 构建生产环境 yarn build:prod
# 前端访问地址 http://localhost:3000
```

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7. 登录日志：系统登录日志记录查询包含登录异常。


## 在线体验

- 账号密码：test/123456  

演示地址：http://company.guiplan.com/adminmanage/index.html

前端文档地址：http://doc.ruoyi.vip

## 演示图

<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/cd1f90be5f2684f4560c9519c0f2a232ee8.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/1cbcf0e6f257c7d3a063c0e3f2ff989e4b3.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-936ec82d1f4872e1bc980927654b6007307.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d67451d308b7a79ad6819723396f7c3d77a.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/5e8c387724954459291aafd5eb52b456f53.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/644e78da53c2e92a95dfda4f76e6d117c4b.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8370a0d02977eebf6dbf854c8450293c937.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-49003ed83f60f633e7153609a53a2b644f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d4fe726319ece268d4746602c39cffc0621.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c195234bbcd30be6927f037a6755e6ab69c.png"/></td>
    </tr>
</table>


# 二、后端步骤
## 1.  安装后端插件
后端代码全部放在servers文件夹中
打开这个servers文件夹目录，然后输入以下命令即可安装

```npm install ```
## 2.启动后端服务
<span style="color:red">注意：后端服务启动需要nodemon插件支持，nodemon插件作用是每次修改后端代码即可自动重启后端服务。</span>
可用以下命令全局安装nodemon
```npm install nodemon -g```
启动后端服务输入以下命令
```npm run dev```
## 3.数据库连接配置
koajsAdmin用的是mongodb数据，所以电脑需要<span style="color:red">安装mongodb数据库</span>。
这是mongodb下载地址 [https://www.mongodb.com/try/download/community](https://www.mongodb.com/try/download/community)
在.env文件夹中可以修改各种配置
详情请看以下注释

```
NODE_ENV=dev
SERVER_PORT=8086   // 服务端口号
DB_HOST=localhost  // 数据库地址
DB_NAME=koaadmin  // 数据库名称
DB_USER=   // 数据库用户名，无需密码登录可不填写
DB_PASSWORD= // 数据库密码  无需密码登录可不填写
DB_PORT=27017   // 数据库端口号
JWT_SECRET=245509608@qq.com // jwt校验码
JWT_EXPIRE=7d // jwt校验，目前用于登录的过期时间，d为天  7d则表示登录有效期为7天
STATIC_PATH=statics // 服务器静态资源地址
AUTH=HSKAHDJSODURUEE // 加密密钥，不同的项目可设置不同的加密方式
```

## 后端打包
很多node环境下的框架如：express,koa等都没考虑服务器部署问题，所以本人用webpack将后端代码打包也配置好了，开箱即用。注意要安装webpack。
servers文件夹输入打包命令为  
 ``` webpack --config webpack.config.js ```   
 .env.production 文件即可配置打包之后的数据库等配置信息。比如服务器的数据需要输入密码，本地的不需要密码，只需.evn与.env.production分别修改区分即可。

或severs文件夹里输入一下命令
``` npm run build ```


# 注册与登录
## 1.注册
数据库连接成功之后，我们就需要一个管理员账户。    
前后端服务都启动之后在浏览器输入地址 http://locahost:3000 打开页面     
然后点击下图位置进入注册页面     
输入账户密码即可完成注册。
注意：管理员账户默认注册只支持admin。      
为了后台数据的安全管理员账户不可随意注册。     
![在这里插入图片描述](https://img-blog.csdnimg.cn/3cc691c3833646fca74995d58f5e784e.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2Vi5YmN56uv56We5Zmo,size_20,color_FFFFFF,t_70,g_se,x_16)
如果管理员账户想用其他名字可找到以下文件直接修改代码即可
![在这里插入图片描述](https://img-blog.csdnimg.cn/f05472b56e6047a6b1718a5a47725ca6.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAd2Vi5YmN56uv56We5Zmo,size_20,color_FFFFFF,t_70,g_se,x_16)
## 2. 登录
最后用注册好的账户登录即可，这样整个项目的前后端就全部打通了。      
后续丰富页面与接口我们就可以利用guiplan可视化低代码开发工具来快速开发。   
也可以自己手写代码二次开发。        
具体视频教程可查阅官网。  

# 3.教程

更多可视化教程可见官网，包括布局，小程序，手写代码转可视化等教程。
https://www.guiplan.com

## 可视化全栈视频教程

完整koajsAdmin全栈教程如下

https://www.bilibili.com/video/BV1vB4y1U7bJ/?spm_id_from=333.999.0.0&vd_source=6c5683ff2482bbae011fc2aa00abe585


## 更新记录
v2.0.0 修复后端文件上传之后临时缓存未清理的问题，添加了打包命令。添加了vscode后端环境判断，后端可通过vscode启动来断点。
v1.0.1 修复菜单无法排序问题，数据库插件添加count查询数量方法，并修复插件让数据库插件查询到的数据支持修改。mongoose插件修改之后校验中间件也做了调整。后台添加了数据库的备份与还原功能，以及后台重置密码可用明文输入。保存之后自动进行加密。


